import gulp from 'gulp';
import browserSync from 'browser-sync';
import sourcemaps from 'gulp-sourcemaps';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import concat from 'gulp-concat';
import autoprefixer from 'gulp-autoprefixer';
import cleanCss from 'gulp-clean-css';
import uglify from 'gulp-uglify';

browserSync.create();
const sass = gulpSass(dartSass);

export const browsersync = () => {
  browserSync.init({
    server: { baseDir: 'dist/' },
    notify: false,
  });
};

export const pages = () => {
  return gulp.src('src/**/*.html').pipe(gulp.dest('dist'));
};

export const styles = () => {
  return gulp.src('src/scss/styles.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(concat('styles.min.css'))
    .pipe(autoprefixer())
    .pipe(cleanCss({ level: 2 }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/css'))
    .pipe(browserSync.stream());
};

export const scripts = () => {
  return gulp.src('src/js/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(concat('scripts.min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/js'))
    .pipe(browserSync.stream());
};

export const vendor = () => {
  return gulp.src(['node_modules/jquery/dist/jquery.js'])
    .pipe(concat('vendor.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));
};

export const watching = () => {
  gulp.watch('src/scss/**/*.scss', styles);
  gulp.watch('src/js/**/*.js', scripts);
  gulp.watch('src/**/*.html').on('change', gulp.series(pages, browserSync.reload));
};

export default gulp.parallel(pages, styles, scripts, vendor, browsersync, watching);
