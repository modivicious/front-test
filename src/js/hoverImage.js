(function () {
  document.querySelectorAll('.js-img-hover').forEach(img => {
    const imgParent = img.parentNode;
    let interval = null;

    img.onmouseover = () => {
      const toTop = 0.33 * imgParent.offsetHeight;
      const toRight = 0.66 * imgParent.offsetWidth;
      const toBottom = 0.66 * imgParent.offsetHeight;
      const toLeft = 0.33 * imgParent.offsetWidth;

      const bottomBorder = -(img.offsetHeight - imgParent.offsetHeight);
      const rightBorder = -(img.offsetWidth - imgParent.offsetWidth);

      prevMovingX = 0;
      prevMovingY = 0;

      const onMouseMove = (event) => {
        const currImagePos = (img.style.objectPosition || '0 0').split(' ');
        let currImageX = parseInt(currImagePos[0]);
        let currImageY = parseInt(currImagePos[1]);

        const mouseX = event.offsetX;
        const mouseY = event.offsetY;

        let currMovingX = 0;
        let currMovingY = 0;

        if (mouseX > toRight) currMovingX = -100;
        else if (mouseX < toLeft) currMovingX = 100;
        else currMovingX = 0;

        if (mouseY > toBottom) currMovingY = -100;
        else if (mouseY < toTop) currMovingY = 100;
        else currMovingY = 0;

        // if direction of movement has changed, then update interval
        if (prevMovingX !== currMovingX || prevMovingY !== currMovingY) {
          clearInterval(interval);
          interval = setInterval(() => {

            // if movement is beyond borders of image, then assign value of border
            if (currImageX >= -currMovingX && currMovingX === 100) {
              currMovingX = 0;
              currImageX = 0;
            }
            if (currImageX <= rightBorder - currMovingX && currMovingX === -100) {
              currMovingX = 0;
              currImageX = rightBorder;
            }
            if (currImageY >= -currMovingY && currMovingY === 100) {
              currMovingY = 0;
              currImageY = 0;
            }
            if (currImageY <= bottomBorder - currMovingY && currMovingY === -100) {
              currMovingY = 0;
              currImageY = bottomBorder;
            }

            // every N ms of holding cursor, increase coordinates
            currImageX += currMovingX;
            currImageY += currMovingY;

            img.style.objectPosition = `${currImageX}px ${currImageY}px`;

            if (currMovingX === 0 && currMovingY === 0) {
              clearInterval(interval);
            }
          }, 180);

          prevMovingX = currMovingX;
          prevMovingY = currMovingY;
        }
      };

      img.addEventListener('mousemove', onMouseMove);

      img.onmouseout = () => {
        clearInterval(interval);
        img.removeEventListener('mousemove', onMouseMove);
        img.onmouseout = null;
      }
    };
  });

})();