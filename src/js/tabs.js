(function ($) {
    'use strict';

    /**
     * Табы
     */
    $.fn.tabs = function () {
        var $self = $(this);
        var $tabHeaders = $self.find('.js-tab-header').filter(function (index, el) {
            return $(el).parentsUntil($self).length === 1;
        });
        var $tabContent = $self.find('.js-tab-content').filter(function (index, el) {
            return $(el).parentsUntil($self).length === 1;
        });

        var $tabHeaderWrap = $self.find('.js-tab-header-wrap');
        var $tabMobileBtn = $self.find('.js-tab-modile-btn');

        /**
         * Активация таба по его индексу
         * @param {Number} index - индекс таба, который нужно активировать
         */
        var selectTab = function (index) {
            $tabHeaders.removeClass('active').eq(index).addClass('active');
            $tabContent.removeClass('active').eq(index).addClass('active');

            $tabMobileBtn.find('span').text($tabHeaders.eq(index).text());
        };

        /**
         * Обработка событий на мобилке
         */
        var mobileInit = function () {
            if (window.matchMedia('(max-width: 767px)').matches) {
                $tabMobileBtn.on('click', function () {
                    $tabHeaderWrap.toggleClass('open');
                    $(this).toggleClass('active');
                })

                $(document).on('click', function (e) {
                    if (!$tabMobileBtn.is(e.target) && $tabMobileBtn.has(e.target).length === 0) {
                        $tabHeaderWrap.removeClass('open');
                        $tabMobileBtn.removeClass('active');
                    }
                })
            }
        }

        /**
         * Инициализаиця
         */
        var init = function () {
            selectTab(0);

            // Обработка событий
            $tabHeaders.on('click', function () {
                selectTab($(this).index());
            });

            mobileInit();
        };

        init();

        this.selectTab = selectTab;

        return this;
    };

    // Инициализируем табы на всех блоках с классом 'js-tabs'
    $('.js-tabs').each(function () {
        $(this).data('tabs', $(this).tabs());
    });
})(jQuery);
