(function () {
  document.querySelectorAll('.js-img-drag').forEach(img => {
    const imgParent = img.parentNode;

    img.onmousedown = (e) => {
      e.preventDefault();

      const bottomBorder = -(img.offsetHeight - imgParent.offsetHeight);
      const rightBorder = -(img.offsetWidth - imgParent.offsetWidth);

      const moveAt = (moveX, moveY) => {
        const currImagePos = (img.style.objectPosition || '0 0').split(' ');
        const currImageX = parseInt(currImagePos[0]);
        const currImageY = parseInt(currImagePos[1]);

        const clickX = e.offsetX || e.layerX;
        const clickY = e.offsetY || e.layerY;

        let newX = currImageX - (clickX - moveX);
        let newY = currImageY - (clickY - moveY);

        // if movement is beyond borders of image, then assign value of border
        if (newY > 0) newY = 0;
        else if (newY < bottomBorder) newY = bottomBorder;
        if (newX > 0) newX = 0;
        else if (newX < rightBorder) newX = rightBorder;

        img.style.objectPosition = `${newX}px ${newY}px`;
      };

      const onMouseMove = (event) => {
        moveAt(event.offsetX, event.offsetY);
      };

      img.addEventListener('mousemove', onMouseMove);

      img.onmouseout = () => {
        img.removeEventListener('mousemove', onMouseMove);
        img.onmouseup = null;
        img.onmouseout = null;
      };

      img.onmouseup = () => {
        img.removeEventListener('mousemove', onMouseMove);
        img.onmouseout = null;
        img.onmouseup = null;
      };
    };
  });

})();
