(function () {
  const headerNav = document.querySelector('.header-nav');
  const headerBtn = document.querySelector('.header-btn');

  const toggleMenu = () => {
    headerNav.classList.toggle('active');
    headerBtn.classList.toggle('active');
  };

  headerBtn.addEventListener('click', toggleMenu);

  document.addEventListener('click', e => {
    if (
      headerNav.classList.contains('active')
      && !headerNav.contains(e.target)
      && !headerBtn.contains(e.target)
    ) toggleMenu();
  });

})();