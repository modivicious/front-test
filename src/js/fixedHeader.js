(function () {
  const header = document.querySelector(".header");
  const logo = document.querySelector(".header-logo svg");
  const main = document.querySelector(".main");

  window.addEventListener("scroll", () => {
    if (window.pageYOffset > 0) {
      header.classList.add("header--fixed");
      logo.classList.add("decreaseWidth");
      main.classList.add("balancePadding");
    }
    else {
      header.classList.remove("header--fixed");
      logo.classList.remove("decreaseWidth");
      main.classList.remove("balancePadding");
    }
  });
})();
